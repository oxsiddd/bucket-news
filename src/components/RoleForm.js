import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import TableRole from 'src/components/TableRole';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '48ch',
    },
  },
  btnAdd: {
    borderRadius: '20px',
    background: '#FFE584',
    float: 'right',
    marginTop: '30px',
    marginBottom: '20px'
  },
  tableWrapper: {
    padding: '10px'
  }
}));

export default function RoleForm(props) {
  const { title } = props;
  const classes = useStyles();

  return (
    <div>
      <h2>{title}</h2>
      <form className={classes.root} noValidate autoComplete="off">
        <div>
          <TextField
            id="standard-textarea"
            label="ชื่อสิทธิ์การเข้าถึง"
            placeholder="ระบุชื่อสิทธิ์การเข้าถึง"
            variant="outlined"
          />
        </div>
        <div className={classes.tableWrapper}>
          <TableRole />
        </div>
      </form>
      <Button variant="contained" className={classes.btnAdd}>เพิ่มสิทธิ์การเข้าถึง</Button>
    </div>
  );
}
