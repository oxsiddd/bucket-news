import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const currencies = [
  {
    value: 'John Doe',
    label: 'John Doe',
  },
  {
    value: 'Jane Doe',
    label: 'Jane Doe',
  },
];

const tags = [
  {
    value: 'สังคม',
    label: 'สังคม',
  },
  {
    value: 'บันเทิง',
    label: 'บันเทิง',
  },
  {
    value: 'เศรษฐกิจ',
    label: 'เศรษฐกิจ',
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '48ch',
    },
  },
  btnAdd: {
    borderRadius: '20px',
    background: '#FFE584',
    float: 'right',
    marginTop: '30px',
    marginBottom: '20px'
  },
}));

export default function ExampleTextFields(props) {
  const { title } = props;
  const classes = useStyles();
  const [currency, setCurrency] = React.useState('');
  const [currency2, setCurrency2] = React.useState('');
  const [tag, setTag] = React.useState('');

  const handleChangeSelect = (event) => {
    setCurrency(event.target.value);
  };

  const handleChangeSelect2 = (event) => {
    setCurrency2(event.target.value);
  };

  const handleChangeTag = (event) => {
    setTag(event.target.value);
  };

  return (
    <div>
      <h2>{title}</h2>
      <form className={classes.root} noValidate autoComplete="off">
        <div>
          <TextField
            id="standard-textarea"
            label="ชื่อหมายข่าว"
            placeholder="ระบุชื่อหมายข่าว"
            multiline
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="filled-multiline-flexible"
            label="สถานที่"
            placeholder="ระบุสถานที่"
            multiline
            rowsMax={6}
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="filled-multiline-flexible"
            label="ประเด็น"
            placeholder="ระบุประเด็น"
            multiline
            rowsMax={6}
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="filled-multiline-flexible"
            label="คำถาม"
            placeholder="ระบุคำถาม"
            multiline
            rowsMax={6}
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="filled-multiline-flexible"
            label="รายละเอียดหมายข่าว"
            placeholder="ระบุรายละเอียดหมายข่าว"
            multiline
            rowsMax={6}
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="standard-select-reporter"
            select
            label="แท็กที่เกี่ยวข้อง"
            variant="outlined"
            value={tag}
            onChange={handleChangeTag}
          >
            {tags.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
        <div>
          <h3>ผู้รับมอบหมาย</h3>
        </div>
        <div>
          <TextField
            id="standard-select-reporter"
            select
            label="นักข่าว"
            value={currency}
            onChange={handleChangeSelect}
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
        <div>
          <TextField
            id="standard-select-camera"
            select
            label="ช่างภาพ"
            value={currency2}
            onChange={handleChangeSelect2}
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </form>
      <Button variant="contained" className={classes.btnAdd}>เพิ่มหมายข่าว</Button>
    </div>
  );
}
