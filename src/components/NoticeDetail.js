import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import WorkIcon from '@material-ui/icons/Work';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';
import ThumbnailImg from 'src/assets/images/thumbnail-img.svg';
import ThumbnailVdo from 'src/assets/images/thumbnail-vdo.svg';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  full: {
    width: '100%',
    padding: '10px 20px'
  },
  mediaBox: {
    display: 'inline-flex',
    listStyle: 'none'
  },
  mediaBoxItem: {
    margin: 5
  },
  table: {
    minWidth: 600,
  },
}));

function createData(name, status, manage) {
  return { name, status, manage };
}

const rows = [
  createData('นักข่าว: Jane D.', 'เสร็จ', <Button variant="contained" size="small">แก้ไขเนื้อหาข่าว</Button>),
  createData('ช่างภาพ: John D.', 'เสร็จ', <Button variant="contained" size="small">แก้ไขรูปภาพหรือวีดิโอ</Button>),
];

export default function NoticeDetail() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List className={classes.full}>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <WorkIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={<h2>หมายข่าว สังคม 01</h2>} />
        </ListItem>
      </List>
      <div className={classes.full}>
        <h3>สถานที่</h3>
        <p>999/9 ถนน พระรามที่ ๑ แขวง ปทุมวัน เขตปทุมวัน กรุงเทพมหานคร 10330</p>
      </div>
      <div className={classes.full}>
        <h3>ประเด็นหมายข่าว</h3>
        <p>Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16</p>
      </div>
      <div className={classes.full}>
        <h3>คำถามที่เกี่ยวกับหมายข่าว</h3>
        <ul>
          <li>ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16</li>
          <li>ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16</li>
          <li>Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ </li>
        </ul>
      </div>
      <div className={classes.full}>
        <h3>รายละเอียดหมายข่าว</h3>
        <p>Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16</p>
      </div>
      <div className={classes.full}>
        <h3>คำที่เกี่ยวข้อง</h3>
        <div className={classes.root}>
          <Chip size="small" label="Basic" />
          <Chip size="small" avatar={<Avatar>M</Avatar>} label="Clickable" />
          <Chip
            size="small"
            avatar={<Avatar alt="Natacha" src="/static/images/avatar/1.jpg" />}
            label="Deletable"
          />
          <Chip
            size="small"
            label="Clickable Deletable"
          />
          <Chip
            size="small"
            label="Custom delete icon"
            deleteIcon={<DoneIcon />}
          />
          <Chip size="small" label="Clickable Link" component="a" href="#chip" clickable />
        </div>
      </div>
      <div className={classes.full}>
        <Table className={classes.table} aria-label="caption table">
          <TableHead>
            <TableRow>
              <TableCell>ผู้รับมอบหมาย</TableCell>
              <TableCell align="right">สถานะการดำเนินการ</TableCell>
              <TableCell align="left">จัดการ</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.status}</TableCell>
                <TableCell align="left">{row.manage}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      <div className={classes.full}>
        <h3>รูปภาพที่เกี่ยวข้อง</h3>
        <ul className={classes.mediaBox}>
          {[...Array(4)].map(index => <li key={index} className={classes.mediaBoxItem}><img src={ThumbnailImg} alt={index} /></li>)}
        </ul>
      </div>

      <div className={classes.full}>
        <h3>วีดิโอที่เกี่ยวข้อง</h3>
        <ul className={classes.mediaBox}>
          {[...Array(2)].map(index => <li key={index} className={classes.mediaBoxItem}><img src={ThumbnailVdo} alt={index} /></li>)}
        </ul>
      </div>

    </div>
  );
}
