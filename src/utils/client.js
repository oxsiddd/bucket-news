import axios from 'axios';
import { to } from 'await-to-js';
import { API_URL } from 'src/constants';
import qs from 'qs';

const client = axios.create({
  baseURL: API_URL,
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  },
  paramsSerializer: params => {
    return qs.stringify(params, {
      encodeValuesOnly: true,
      arrayFormat: 'brackets'
    });
  }
});

export default {
  fetch: client,
  get: async (path, options = {}, defaultData = []) => {
    const [err, res] = await to(client.get(path, options));
    return err ? defaultData : res.data;
  },
  post: async (path, payload, options = {}, defaultData = []) => {
    const [err, res] = await to(client.post(path, payload, options));
    return err ? defaultData : res.data;
  },
  put: async (path, payload, options = {}, defaultData = []) => {
    const [err, res] = await to(client.put(path, payload, options));
    return err ? defaultData : res.data;
  },
  delete: async (path, options = {}, defaultData = []) => {
    const [err, res] = await to(client.delete(path, options));
    return err ? defaultData : res.data;
  }
};
