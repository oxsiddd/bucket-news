import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const position = [
  {
    value: 'CEO',
    label: 'CEO',
  },
  {
    value: 'AE',
    label: 'AE',
  },
  {
    value: 'BA',
    label: 'BA',
  },
];

const role = [
  {
    value: 'บก.',
    label: 'บก.',
  },
  {
    value: 'นักข่าว',
    label: 'นักข่าว',
  },
  {
    value: 'ช่างภาพ',
    label: 'ช่างภาพ',
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '48ch',
    },
  },
  btnAdd: {
    borderRadius: '20px',
    background: '#FFE584',
    float: 'right',
    marginTop: '30px',
    marginBottom: '20px'
  },
}));

export default function UserForm(props) {
  const { title } = props;
  const classes = useStyles();
  const [currency, setCurrency] = React.useState('');
  const [currency2, setCurrency2] = React.useState('');

  const handleChangeSelect = (event) => {
    setCurrency(event.target.value);
  };

  const handleChangeSelect2 = (event) => {
    setCurrency2(event.target.value);
  };

  return (
    <div>
      <h2>{title}</h2>
      <form className={classes.root} noValidate autoComplete="off">
        <div>
          <TextField
            id="standard-textarea"
            label="ชื่อ-นามสกุล"
            placeholder="ระบุชื่อ-นามสกุล"
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="filled-multiline-flexible"
            label="อีเมล"
            placeholder="ระบุอีเมล"
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="standard-select-reporter"
            select
            label="ตำแหน่ง"
            value={currency}
            onChange={handleChangeSelect}
            variant="outlined"
          >
            {position.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
        <div>
          <TextField
            id="standard-select-camera"
            select
            label="สิทธิ์การเข้าถึง"
            value={currency2}
            onChange={handleChangeSelect2}
            variant="outlined"
          >
            {role.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </form>
      <Button variant="contained" className={classes.btnAdd}>เพิ่มผู้ใช้งาน</Button>
    </div>
  );
}
