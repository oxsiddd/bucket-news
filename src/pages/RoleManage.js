import Layout from 'src/components/Layout';
import TableManageRole from 'src/components/TableManageRole';

const RoleManage = () => {
  return (
    <Layout>
      <TableManageRole />
    </Layout>
  );
}

export default RoleManage;
