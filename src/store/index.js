import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import rootReducers from 'src/store/reducers'

export default function configureStore() {
  return createStore (
    rootReducers,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  )
}
