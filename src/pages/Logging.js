import Layout from 'src/components/Layout';
import TableLogging from 'src/components/TableLogging';

const Logging = () => {
  return (
    <Layout>
      <TableLogging />
    </Layout>
  );
}

export default Logging;
