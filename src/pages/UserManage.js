import Layout from 'src/components/Layout';
import TableManageUser from 'src/components/TableManageUser';

const UserManage = () => {
  return (
    <Layout>
      <TableManageUser />
    </Layout>
  );
}

export default UserManage;
