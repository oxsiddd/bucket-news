import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import PeopleIcon from '@material-ui/icons/People';
import AccountCircle from '@material-ui/icons/AccountCircle';
import DnsRoundedIcon from '@material-ui/icons/DnsRounded';
import PermMediaOutlinedIcon from '@material-ui/icons/PhotoSizeSelectActual';
import FolderSharedIcon from '@material-ui/icons/FolderShared';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ListIcon from '@material-ui/icons/List';
import BeenhereIcon from '@material-ui/icons/Beenhere';
import {
  Link,
  useLocation
} from "react-router-dom";

import Logo from 'src/assets/images/logo.svg';

const categories =  [
  { id: 'ทั้งหมด', icon: <DnsRoundedIcon />, link: '/dashboard' },
  { id: 'จัดการหมายข่าว', icon: <BookmarksIcon />, link: '/notics-news' },
  { id: 'จัดการรูปภาพและวีดิโอ', icon: <PermMediaOutlinedIcon />, link: '/media-manage' },
  { id: 'จัดการผู้ใช้งาน', icon: <PeopleIcon />, link: '/users-manage' },
  { id: 'จัดการสิทธิ์', icon: <FolderSharedIcon />, link: '/permission-manage' },
  { id: 'จัดการสิทธิ์การเข้าถึง', icon: <BeenhereIcon />, link: '/role-manage' },
  { id: 'ประวัติการใช้งาน', icon: <ListIcon />, link: '/logging' },
];

const drawerWidth = 256;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    background: '#fff',
    color: '#000',
    boxShadow: '0px 2px 4px -1px rgb(0 0 0 / 0%), 0px 4px 5px 0px rgb(0 0 0 / 5%), 0px 1px 10px 0px rgb(0 0 0 / 0%)'
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  imgLogo: {
    height: '48px',
    marginLeft: '-15px'
  },
  sectionHeaderRight: {
    position: 'absolute',
    right: '25px',
    display: 'flex',
    alignItems: 'center'
  },
  badgeRole: {
    color: '#000',
    backgroundColor: '#FFE584',
    marginRight: '5px',
    padding: '2px 16px',
    borderRadius: '50px'
  },
  itemActiveItem: {
    color: '#000',
    backgroundColor: 'rgba(0,0,0,.1)',
  },
  itemActiveIcon: {
    color: '#000',
  },
  itemChoose: {
    textDecoration: 'none',
    color: '#757575'
  },
  listGroup: {
    padding: '0px'
  },
  fromBottom: {
    position: 'absolute',
    bottom: 0,
    width: '100%'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(1),
  },
}));

const Layout = ({ children }) => {
  const classes = useStyles();
  const theme = useTheme();
  const { pathname } = useLocation();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <img src={Logo} alt='logo' className={clsx(classes.imgLogo)} />
          <div className={clsx(classes.sectionHeaderRight)}>
            <Typography className={clsx(classes.badgeRole)} variant='subtitle2' noWrap>
              บก.
            </Typography>
            <Typography className='' variant='subtitle1' noWrap>
              John D.
            </Typography>
            <IconButton>
              <AccountCircle />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List className={classes.listGroup}>
          {categories.map((item, index) => (
            <Link key={item.id} to={item.link} className={clsx(classes.itemChoose)}>
              <ListItem button className={clsx(item.link === pathname && classes.itemActiveItem)}>
                <ListItemIcon className={clsx(item.link === pathname && classes.itemActiveIcon)}>{item.icon}</ListItemIcon>
                <ListItemText primary={item.id} />
              </ListItem>
            </Link>
          ))}
        </List>
        <List className={classes.fromBottom}>
          <Divider />
          <ListItem button>
            <ListItemIcon><ExitToAppIcon /></ListItemIcon>
            <ListItemText primary='ออกจากระบบ' />
          </ListItem>
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
}

export default Layout;
