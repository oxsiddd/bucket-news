import Layout from 'src/components/Layout';
import NoticeDetail from 'src/components/NoticeDetail';

const NoticeNewsDetail = () => {
  return (
    <Layout>
      <NoticeDetail />
    </Layout>
  );
}

export default NoticeNewsDetail;
