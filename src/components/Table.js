import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { useHistory } from 'react-router-dom';

const columns = [
  { id: 'no', label: 'ลำดับ', minWidth: 50 },
  { id: 'notice', label: 'หมายข่าว', minWidth: 170 },
  {
    id: 'user',
    label: 'ผู้รับมอบหมาย',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'status',
    label: 'สถานะ',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  }
];

function createData(no, notice, user, status) {
  return { no, notice, user, status };
}

const rows = [
  createData('1','India', 'John Doe', 'ดำเนินการ'),
  createData('2','China', 'John Doe', 'ดำเนินการ'),
  createData('3','Italy', 'John Doe', 'เสร็จ'),
  createData('4','United States', 'John Doe', 'ดำเนินการ'),
  createData('5','Canada', 'John Doe', 'ยกเลิก'),
  createData('6','Australia', 'John Doe', 'ดำเนินการ'),
  createData('7','Germany', 'Jane Doe', 'ดำเนินการ'),
  createData('8','Ireland', 'John Doe', 'เสร็จ'),
  createData('9','Mexico', 'John Doe', 'ดำเนินการ'),
  createData('10','Japan', 'John Doe', 'ดำเนินการ'),
  createData('11','France', 'Jane Doe', 'ดำเนินการ'),
  createData('12','United Kingdom', 'John Doe', 'ดำเนินการ'),
  createData('13','Russia', 'John Doe', 'ดำเนินการ'),
  createData('12','Nigeria', 'Jane Doe', 'ดำเนินการ'),
  createData('15','Brazil', 'John Doe', 'ดำเนินการ'),
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
});

export default function TableNotice() {
  const history = useHistory();
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.no} onClick={() => history.push('/notics-news/detail')}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
