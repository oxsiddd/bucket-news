import { makeStyles } from '@material-ui/core/styles';
import SignInForm from 'src/components/auth/SigninForm';
import TopNewsLogo from 'src/assets/images/logo.svg';

const useStyles = makeStyles(() => ({
  signInWrapper: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  topLogo: {
    margin: 'auto',
    textAlign: 'center',
    '& img': {
      width: '100%',
      maxWidth: '128px',
    }
  },
  formWrapper: {
    background: '#fff',
    marginTop: '15px',
    padding: '30px',
    borderRadius: '20px',
    boxShadow: '2px 3px 7px rgba(0,0,0,0.1)',
    margin: 'auto'
  }
}));

const SignIn = () => {
  const classes = useStyles();

  return (
    <div className={classes.signInWrapper}>
      <div className={classes.topLogo}>
        <img src={TopNewsLogo} alt='Topnews' />
      </div>
      <div className={classes.formWrapper}>
        <SignInForm />
      </div>
    </div>
  );
}

export default SignIn;
