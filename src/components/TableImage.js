import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Avatar from '@material-ui/core/Avatar';
import mockImg from 'src/assets/images/images-mock.jpeg';

const columns = [
  { id: 'no', label: 'ลำดับ', minWidth: 50 },
  { id: 'img', label: 'ตัวอย่าง', minWidth: 200 },
  {
    id: 'notice',
    label: 'หมายข่าวที่เกี่ยวข้อง',
    minWidth: 170,
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'user',
    label: 'ผู้อัพโหลด',
    minWidth: 120,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'type',
    label: 'ประเภท',
    minWidth: 70,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  }
];

function createData(no, img, notice, user, type) {
  return { no, img, notice, user, type };
}

const rows = [
  createData('1', <Avatar alt="Remy Sharp" src={mockImg} />,'India', 'John Doe', 'รูปภาพ'),
  createData('2', <Avatar alt="Remy Sharp" src={mockImg} />,'China', 'John Doe', 'รูปภาพ'),
  createData('3', <Avatar alt="Remy Sharp" src={mockImg} />,'Italy', 'John Doe', 'วีดิโอ'),
  createData('4', <Avatar alt="Remy Sharp" src={mockImg} />,'United States', 'John Doe', 'รูปภาพ'),
  createData('5', <Avatar alt="Remy Sharp" src={mockImg} />,'Canada', 'John Doe', 'วีดิโอ'),
  createData('6', <Avatar alt="Remy Sharp" src={mockImg} />,'Australia', 'John Doe', 'รูปภาพ'),
  createData('7', <Avatar alt="Remy Sharp" src={mockImg} />,'Germany', 'Jane Doe', 'รูปภาพ'),
  createData('8', <Avatar alt="Remy Sharp" src={mockImg} />,'Ireland', 'John Doe', 'รูปภาพ'),
  createData('9', <Avatar alt="Remy Sharp" src={mockImg} />,'Mexico', 'John Doe', 'รูปภาพ'),
  createData('10', <Avatar alt="Remy Sharp" src={mockImg} />,'Japan', 'John Doe', 'รูปภาพ'),
  createData('11', <Avatar alt="Remy Sharp" src={mockImg} />,'France', 'Jane Doe', 'รูปภาพ'),
  createData('12', <Avatar alt="Remy Sharp" src={mockImg} />,'United Kingdom', 'John Doe', 'รูปภาพ'),
  createData('13', <Avatar alt="Remy Sharp" src={mockImg} />,'Russia', 'John Doe', 'รูปภาพ'),
  createData('12', <Avatar alt="Remy Sharp" src={mockImg} />,'Nigeria', 'Jane Doe', 'รูปภาพ'),
  createData('15', <Avatar alt="Remy Sharp" src={mockImg} />,'Brazil', 'John Doe', 'รูปภาพ'),
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
});

export default function TableImage() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
