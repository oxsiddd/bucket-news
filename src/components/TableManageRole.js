import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Checkbox from '@material-ui/core/Checkbox';

import RoleForm from 'src/components/RoleForm';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
  btnWrapper: {
    position: 'absolute',
    marginTop: '10px',
    marginLeft: '10px',
    zIndex: '999',
  },
  btnAdd: {
    borderRadius: '20px',
    background: '#FFE584'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid none',
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function TableManageRole() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = React.useState(false);
  const [openDelete, setOpenDelete] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleOpenDelete = () => {
    setOpenDelete(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenDelete(false);

  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const columns = [
    { id: 'no', label: 'ลำดับ', minWidth: 50 },
    {
      id: 'name',
      label: 'สิทธิ์การเข้าถึง',
      minWidth: 170,
      align: 'left',
    },
    {
      id: 'add',
      label: 'เพิ่ม',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'del',
      label: 'ลบ',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'edit',
      label: 'แก้ไข',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'view',
      label: 'ดู',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'manage',
      label: 'จัดการ',
      minWidth: 120,
      align: 'right',
    }
  ];

  function createData(no, name, add, del, edit, view, manage) {
    return { no, name, add, del, edit, view, manage };
  }

  const rows = [
    createData(
      '1',
      'บก.',
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>
    ),
    createData(
      '2',
      'ช่างภาพ',
      <Checkbox
        defaultChecked
        indeterminate
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData(
      '3',
      'นักข่าว',
      <Checkbox
        defaultChecked
        indeterminate
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <Checkbox
        defaultChecked
        disabled
        color="primary"
      />,
      <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
  ];

  return (
    <Paper className={classes.root}>
      <div className={classes.btnWrapper}>
        <Button variant="contained" className={classes.btnAdd} onClick={handleOpen}>เพิ่มสิทธิ์การเข้าถึง{' '}<AddIcon /></Button>
      </div>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <RoleForm title='เพิ่มสิทธิ์การเข้าถึง' />
          </div>
        </Fade>
      </Modal>

      <Modal
        className={classes.modal}
        open={openDelete}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openDelete}>
          <div className={classes.paper}>
            <h3>ต้องการลบสิทธิ์การเข้าถึงนี้ ใช่ หรือ ไม่?</h3>
            <div style={{ textAlign: 'center' }}>
              <Button variant="contained" size="small" onClick={handleClose}>ตกลง</Button>
              <Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleClose}>ยกเลิก</Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </Paper>
  );
}
