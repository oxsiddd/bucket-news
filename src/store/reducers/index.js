import { combineReducers } from 'redux';
import { authData } from 'src/store/reducers/auth';

export default combineReducers({
  authData
});
