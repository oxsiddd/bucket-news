import Layout from 'src/components/Layout';
import TableNoticeManage from 'src/components/TableManage';

const NoticeNews = () => {
  return (
    <Layout>
      <TableNoticeManage />
    </Layout>
  );
}

export default NoticeNews;
