const initialState = {
  data: null
};

export const authData = (state = initialState, action) => {
  switch (action.type) {
    case 'SIGNIN_DATA':
      return {
        ...state,
        data: action.data
      };
    default:
      return state;
  }
};
