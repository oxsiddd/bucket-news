import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

import UserForm from 'src/components/UserForm';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
  btnAdd: {
    position: 'absolute',
    marginTop: '10px',
    marginLeft: '10px',
    zIndex: '999',
    borderRadius: '20px',
    background: '#FFE584'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid none',
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function TableManageUser() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = React.useState(false);
  const [openDelete, setOpenDelete] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleOpenDelete = () => {
    setOpenDelete(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenDelete(false);

  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const columns = [
    { id: 'no', label: 'ลำดับ', minWidth: 50 },
    {
      id: 'name',
      label: 'ชื่อ-นามสกุล',
      minWidth: 170,
      align: 'right',
    },
    {
      id: 'user',
      label: 'บัญชีผู้ใช้',
      minWidth: 170,
      align: 'right',
    },
    {
      id: 'position',
      label: 'ตำแหน่ง',
      minWidth: 170,
      align: 'right',
    },
    {
      id: 'role',
      label: 'สิทธิ์การเข้าถึง',
      minWidth: 170,
      align: 'right',
    },
    {
      id: 'manage',
      label: 'จัดการ',
      minWidth: 170,
      align: 'right',
    }
  ];

  function createData(no, name, user, position, role, manage) {
    return { no, name, user, position, role, manage};
  }

  const rows = [
    createData('1', 'John Doe', 'john_d', 'CEO', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('2', 'John Doe', 'john_d', 'CTO', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('3', 'John Doe', 'john_d', 'CFO', 'นักข่าว.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('4', 'John Doe', 'john_d', 'AE', 'บก.',  <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('5', 'John Doe', 'john_d', 'AE', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('6', 'John Doe', 'john_d', 'AE', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('7', 'Jane Doe', 'john_d', 'SM', 'นักข่าว', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('8', 'John Doe', 'john_d', 'VP', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('9', 'John Doe', 'john_d', 'CEO', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('10', 'John Doe', 'john_d', 'VP', 'นักข่าว.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('11','Jane Doe', 'john_d', 'VP', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('12','John Doe', 'john_d', 'BA', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('13','John Doe', 'john_d', 'BA', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('12','Jane Doe', 'john_d', 'BA', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
    createData('15','John Doe', 'john_d', 'BA', 'บก.', <><Button variant="contained" size="small">แก้ไข</Button><Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleOpenDelete}>ลบ</Button></>),
  ];

  return (
    <Paper className={classes.root}>
      <Button variant="contained" className={classes.btnAdd} onClick={handleOpen}>เพิ่มผู้ใช้งาน{' '}<AddIcon /></Button>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <UserForm title='เพิ่มผู้ใช้งาน' />
          </div>
        </Fade>
      </Modal>
      <Modal
        className={classes.modal}
        open={openDelete}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openDelete}>
          <div className={classes.paper}>
            <h3>ต้องการลบผู้ใช้งานนี้ ใช่ หรือ ไม่?</h3>
            <div style={{ textAlign: 'center' }}>
              <Button variant="contained" size="small" onClick={handleClose}>ตกลง</Button>
              <Button variant="contained" size="small" color="secondary" style={{ marginLeft: '5px' }} onClick={handleClose}>ยกเลิก</Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </Paper>
  );
}
