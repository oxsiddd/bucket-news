import React from 'react';
import clsx from 'clsx';
import { useHistory } from "react-router-dom";
import { useForm, Controller } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Swal from 'sweetalert2';
import Cookies from 'js-cookie';

import client from 'src/utils/client';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '32ch'
    },
  },
  btnAdd: {
    width: '100%',
    borderRadius: '50px',
    background: 'linear-gradient(200deg, rgba(255,229,132,1) 22%, rgba(93,77,35,1) 53%)',
    margin: '20px auto',
    padding: '15px',
    color: '#fff',
    fontWeight: 'bolder'
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '32ch',
  }
}));

const SignInForm = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { control, handleSubmit } = useForm();
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });

  const onSubmit = async (data) => {
    await client.fetch.post('/api/login', { email: data.user, password: values.password })
      .then(async (res) => {
        console.log(res);
        await Cookies.set('_key', res.data.auth.key);
        await Cookies.set('_id', res.data.auth.id);
        await Cookies.set('_name', res.data.data.name);
        await Cookies.set('_email', res.data.data.email);
        await Cookies.set('_role', res.data.data.role);
        await dispatch({ type: 'SIGNIN_DATA', data: res.data });
        history.push("/dashboard");
      })
      .catch(err => {
        console.log(err.response);
        const { data } = err.response;
        Swal.fire({
          title: 'Error!',
          text: data.message,
          icon: 'error'
        })
      });
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  console.log(values);

  return (
    <div>
      <form className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
        <div>
          <Controller
            name="user"
            control={control}
            render={({ field }) => <TextField
              {...field}
              id="standard-textarea"
              label="บัญชีผู้ใช้"
              placeholder="กรอกบัญชีผู้ใช้"
              variant="outlined"
            />}
            defaultValue=""
          />
        </div>
        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">รหัสผ่าน</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={60}
          />
        </FormControl>
        <div>
          <Button type="submit" variant="contained" className={classes.btnAdd}>เข้าสู่ระบบ</Button>
        </div>
      </form>
    </div>
  );
}

export default SignInForm;
