import Layout from 'src/components/Layout';
import TableNoticeManageImage from 'src/components/TableManageImage';

const MediaManage = () => {
  return (
    <Layout>
      <TableNoticeManageImage />
    </Layout>
  );
}

export default MediaManage;
