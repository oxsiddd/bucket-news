import Layout from 'src/components/Layout';
import TableManagePermission from 'src/components/TableManagePermission';

const PermissionManage = () => {
  return (
    <Layout>
      <TableManagePermission />
    </Layout>
  );
}

export default PermissionManage;
