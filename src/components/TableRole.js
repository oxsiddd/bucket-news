import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
  btnWrapper: {
    position: 'absolute',
    marginTop: '10px',
    marginLeft: '10px',
    zIndex: '999',
  },
  btnAdd: {
    borderRadius: '20px',
    background: '#FFE584'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid none',
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function TableRole() {
  const classes = useStyles();

  const columns = [
    {
      id: 'add',
      label: 'เพิ่ม',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'del',
      label: 'ลบ',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'edit',
      label: 'แก้ไข',
      minWidth: 20,
      align: 'center',
    },
    {
      id: 'view',
      label: 'ดู',
      minWidth: 20,
      align: 'center',
    },
  ];

  function createData( add, del, edit, view ) {
    return { add, del, edit, view };
  }

  const rows = [
    createData(
      <Checkbox
        color="primary"
      />,
      <Checkbox
        defaultChecked
        color="primary"
      />,
      <Checkbox
        defaultChecked
        color="primary"
      />,
      <Checkbox
        defaultChecked
        color="primary"
      />,
    )];

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
