import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const role = [
  {
    value: 'บก.',
    label: 'บก.',
  },
  {
    value: 'นักข่าว',
    label: 'นักข่าว',
  },
  {
    value: 'ช่างภาพ',
    label: 'ช่างภาพ',
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '48ch',
    },
  },
  btnAdd: {
    borderRadius: '20px',
    background: '#FFE584',
    float: 'right',
    marginTop: '30px',
    marginBottom: '20px'
  },
}));

export default function PermissionForm(props) {
  const { title } = props;
  const classes = useStyles();
  const [currency2, setCurrency2] = React.useState('');

  const handleChangeSelect2 = (event) => {
    setCurrency2(event.target.value);
  };

  return (
    <div>
      <h2>{title}</h2>
      <form className={classes.root} noValidate autoComplete="off">
        <div>
          <TextField
            id="standard-textarea"
            label="ชื่อสิทธิ์การเข้าถึง"
            placeholder="ระบุชื่อสิทธิ์การเข้าถึง"
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            id="standard-select-camera"
            select
            label="สิทธิ์การเข้าถึง"
            value={currency2}
            onChange={handleChangeSelect2}
            variant="outlined"
          >
            {role.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </form>
      <Button variant="contained" className={classes.btnAdd}>เพิ่มสิทธิ์การเข้าถึง</Button>
    </div>
  );
}
