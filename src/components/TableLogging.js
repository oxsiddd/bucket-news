import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const columns = [
  { id: 'no', label: 'ลำดับ', minWidth: 50 },
  { id: 'date', label: 'วัน-เวลา', minWidth: 170 },
  { id: 'notice', label: 'หมายข่าว', minWidth: 170 },
  {
    id: 'user',
    label: 'ผู้ใช้งาน',
    minWidth: 170,
    align: 'right',
  },
  {
    id: 'action',
    label: 'การกระทำ',
    minWidth: 170,
    align: 'right',
  }
];

function createData(no, date, notice, user, action) {
  return { no, date, notice, user, action };
}

const rows = [
  createData('1', '2/3/2564 - 10:03', 'India', 'John Doe', 'เพิ่ม'),
  createData('2', '2/3/2564 - 10:03', 'China', 'John Doe', 'เพิ่ม'),
  createData('3', '2/3/2564 - 10:03', 'Italy', 'John Doe', 'เพิ่ม'),
  createData('4', '2/3/2564 - 10:03', 'United States', 'John Doe', 'แก้ไข'),
  createData('5', '2/3/2564 - 10:03', 'Canada', 'John Doe', 'ลบ'),
  createData('6', '2/3/2564 - 10:03', 'Australia', 'John Doe', 'แก้ไข'),
  createData('7', '2/3/2564 - 10:03', 'Germany', 'Jane Doe', 'แก้ไข'),
  createData('8', '2/3/2564 - 10:03', 'Ireland', 'John Doe', 'ลบ'),
  createData('9', '2/3/2564 - 10:03', 'Mexico', 'John Doe', 'ลบ'),
  createData('10', '2/3/2564 - 10:03', 'Japan', 'John Doe', 'เพิ่ม'),
  createData('11', '2/3/2564 - 10:03', 'France', 'Jane Doe', 'เพิ่ม'),
  createData('12', '2/3/2564 - 10:03', 'United Kingdom', 'John Doe', 'แก้ไข'),
  createData('13', '2/3/2564 - 10:03', 'Russia', 'John Doe', 'แก้ไข'),
  createData('12', '2/3/2564 - 10:03', 'Nigeria', 'Jane Doe', 'แก้ไข'),
  createData('15', '2/3/2564 - 10:03', 'Brazil', 'John Doe', 'ลบ'),
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
});

export default function TableLogging() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.no}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
