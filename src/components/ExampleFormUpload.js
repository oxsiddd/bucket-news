import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ChooseImg from 'src/assets/images/choose-img.svg';
import ChooseVdo from 'src/assets/images/choose-vdo.svg';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '48ch',
    },
  },
  chooseBox: {
    width: '56ch',
    display: 'flex',
    justifyContent: 'center'
  },
  chooseBoxItem: {
    width: '50%',
  }
}));

export default function ExampleTextFields(props) {
  const { title } = props;
  const classes = useStyles();

  return (
    <div>
      <h2>{title}</h2>
      <div className={classes.chooseBox}>
        <div className={classes.chooseBoxItem}>
          <h3>อัพโหลดรูปภาพ</h3>
          <img src={ChooseImg} alt='img' />
        </div>
        <div>
          <h3>อัพโหลดวีดิโอ</h3>
          <img src={ChooseVdo} alt='img' />
        </div>
      </div>
    </div>
  );
}
