import React from 'react';
import { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Cookies from 'js-cookie';
import Dashboard from 'src/pages/Dashboard';
import NoticeNews from 'src/pages/NoticeNews';
import NoticeNewsDetail from 'src/pages/NoticeNewsDetail';
import MediaManage from 'src/pages/MediaManage';
import UserManage from 'src/pages/UserManage';
import PermissionManage from 'src/pages/PermissionManage';
import RoleManage from 'src/pages/RoleManage';
import Logging from 'src/pages/Logging';
import SignIn from 'src/pages/Signin';
import SignOut from 'src/pages/Signout';

const App = ({ loggedIn }) => {

  console.log(Cookies.get('_key'));

  return (
    <Router>
      <Switch>
        <Route path='/dashboard'>
          <Dashboard />
        </Route>
        <Route path='/notics-news/detail'>
          <NoticeNewsDetail />
        </Route>
        <Route path='/notics-news'>
          <NoticeNews />
        </Route>
        <Route path='/media-manage'>
          <MediaManage />
        </Route>
        <Route path='/users-manage'>
          <UserManage />
        </Route>
        <Route path='/permission-manage'>
          <PermissionManage />
        </Route>
        <Route path='/role-manage'>
          <RoleManage />
        </Route>
        <Route path='/logging'>
          <Logging />
        </Route>
        <Route path='/signout'>
          <SignOut />
        </Route>
        <Route path='/'>
          <SignIn />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
